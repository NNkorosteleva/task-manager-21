package ru.tsc.korosteleva.tm.command.project;

import ru.tsc.korosteleva.tm.api.service.IProjectService;
import ru.tsc.korosteleva.tm.api.service.IProjectTaskService;
import ru.tsc.korosteleva.tm.command.AbstractCommand;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Project;
import ru.tsc.korosteleva.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
    }

}
