package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.model.User;

import java.util.List;

public class UserFindAllCommand extends AbstractUserCommand {

    public static final String NAME = "user-find-all";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Find all users.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[FIND ALL USERS]");
        final List<User> users = getUserService().findAll();
        renderUsers(users);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
