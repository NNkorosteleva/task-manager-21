package ru.tsc.korosteleva.tm.repository;

import ru.tsc.korosteleva.tm.api.repository.IUserRepository;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login,
                       final String password,
                       final String email) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User create(final String login,
                       final String password,
                       final String email,
                       final String role) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        Role roleEnum = Role.toRole(role);
        user.setRole(roleEnum);
        return add(user);
    }

    @Override
    public User updateUser(final String id,
                           final String firstName,
                           final String lastName,
                           final String middleName) {
        final User user = findOneById(id);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User setPassword(final String id, final String password) {
        final User user = findOneById(id);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User findOneByLogin(final String login) {
        return records.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return records.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findOneByLogin(login);
        records.remove(user);
        return user;
    }

}
