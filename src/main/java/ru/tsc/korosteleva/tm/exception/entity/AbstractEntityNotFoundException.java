package ru.tsc.korosteleva.tm.exception.entity;

import ru.tsc.korosteleva.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
    }

    public AbstractEntityNotFoundException(String message) {
        super(message);
    }

}
