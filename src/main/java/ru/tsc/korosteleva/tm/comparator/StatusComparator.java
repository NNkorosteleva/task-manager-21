package ru.tsc.korosteleva.tm.comparator;

import ru.tsc.korosteleva.tm.api.model.IHasStatus;

import java.util.Comparator;

public enum StatusComparator implements Comparator<IHasStatus> {

    INSTANCE;

    @Override
    public int compare(final IHasStatus status1, final IHasStatus status2) {
        if (status1 == null || status2 == null) return 0;
        if (status1.getStatus() == null || status2.getStatus() == null) return 0;
        return status1.getStatus().compareTo(status2.getStatus());
    }

}
