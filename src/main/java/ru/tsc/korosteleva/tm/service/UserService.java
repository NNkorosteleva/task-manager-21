package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IUserRepository;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.exception.entity.UserNotFoundException;
import ru.tsc.korosteleva.tm.exception.user.*;
import ru.tsc.korosteleva.tm.model.User;

import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(final IUserRepository repository) {
        super(repository);
    }

    @Override
    public User create(final String login,
                       final String password,
                       final String email) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        Optional.of(login).filter(l -> !isLoginExist(login)).orElseThrow(() -> new LoginExistException(login));
        Optional.of(email).filter(e -> !isEmailExist(email)).orElseThrow(() -> new EmailExistException(email));
        return repository.create(login, password, email);
    }

    @Override
    public User create(final String login,
                       final String password,
                       final String email,
                       final String role) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        Optional.of(login).filter(l -> !isLoginExist(login)).orElseThrow(() -> new LoginExistException(login));
        Optional.of(email).filter(e -> !isEmailExist(email)).orElseThrow(() -> new EmailExistException(email));
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        Optional.of(role).filter(r -> !isRoleIncorrect(role)).orElseThrow(() -> new RoleIncorrectException(role));
        return repository.create(login, password, email, role);
    }

    @Override
    public User updateUser(final String id,
                           final String firstName,
                           final String lastName,
                           final String middleName) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return Optional.ofNullable(repository.updateUser(id, firstName, lastName, middleName))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User setPassword(final String id, final String password) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        return Optional.ofNullable(repository.setPassword(id, password))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User findOneByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        return repository.findOneByLogin(login);
    }

    @Override
    public User findOneByEmail(final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        return repository.findOneByEmail(email);
    }

    @Override
    public User removeByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        return Optional.ofNullable(repository.removeByLogin(login))
                .orElseThrow(UserNotFoundException::new);
    }

    private boolean isEmailExist(final String email) {
        return findOneByEmail(email) != null;
    }

    private boolean isLoginExist(final String login) {
        return findOneByLogin(login) != null;
    }

    private boolean isRoleIncorrect(String role) {
        return Role.toRole(role) == null;
    }

}
