package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IProjectTaskRepository;
import ru.tsc.korosteleva.tm.api.service.IProjectTaskService;
import ru.tsc.korosteleva.tm.exception.field.IdEmptyException;
import ru.tsc.korosteleva.tm.exception.user.UserIdEmptyException;

import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    final IProjectTaskRepository projectTaskRepository;

    public ProjectTaskService(final IProjectTaskRepository projectTaskRepository) {
        this.projectTaskRepository = projectTaskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId,
                                  final String projectId,
                                  final String taskId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        projectTaskRepository.bindTaskToProject(userId, projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject(final String userId,
                                      final String projectId,
                                      final String taskId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        projectTaskRepository.unbindTaskFromProject(userId, projectId, taskId);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        projectTaskRepository.removeProjectById(userId, projectId);
    }
}
