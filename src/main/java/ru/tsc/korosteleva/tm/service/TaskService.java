package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.api.service.ITaskService;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.entity.TaskNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.*;
import ru.tsc.korosteleva.tm.exception.user.UserIdEmptyException;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId,
                       final String name,
                       final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(final String userId,
                       final String name,
                       final String description,
                       final Date dateBegin,
                       final Date dateEnd) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        Optional.ofNullable(dateBegin).orElseThrow(DateIncorrectException::new);
        Optional.ofNullable(dateEnd).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description, dateBegin, dateEnd);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return Optional.ofNullable(repository.findOneByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task updateById(final String userId,
                           final String id,
                           final String name,
                           final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return Optional.ofNullable(repository.updateById(userId, id, name, description))
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task updateByIndex(final String userId,
                              final Integer index,
                              final String name,
                              final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize(userId))
                .orElseThrow(IndexIncorrectException::new);
        return Optional.ofNullable(repository.updateByIndex(userId, index, name, description))
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task removeByName(final String userId,
                             final String name) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return Optional.ofNullable(repository.removeByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task changeTaskStatusById(final String userId,
                                     final String id,
                                     final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return Optional.ofNullable(repository.changeTaskStatusById(userId, id, status))
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId,
                                        final Integer index,
                                        final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize(userId))
                .orElseThrow(IndexIncorrectException::new);
        return Optional.ofNullable(repository.changeTaskStatusByIndex(userId, index, status))
                .orElseThrow(TaskNotFoundException::new);
    }

}